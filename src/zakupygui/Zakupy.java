// ***************************************************************
//   Zakupy.java
//
//   Wykorzystuje klase Item do stworzenia przedmiotów i dodania ich do koszyka
//   przechowywanego w ArrayList.
// ***************************************************************
package zakupygui;

import java.util.ArrayList;
import java.util.Scanner;
//import zakupygui.Item;
//import cs1.Keyboard;

public class Zakupy {

    public static void main(String[] args) {
        ArrayList<Item> koszyk = new ArrayList<Item>();

        Scanner klawiatura = new Scanner(System.in);

        String nazwaRzeczy;
        double cenaRzeczy;
        int ilosc;
        Item rzecz;

        String kontynuujZakupy = "t";

        while (kontynuujZakupy.equals("t")) {

            System.out.println("Podaj nazwe rzeczy: ");
            nazwaRzeczy = klawiatura.next();

            System.out.println("Podaj cene jednostkowa: ");
            cenaRzeczy = klawiatura.nextDouble();

            System.out.println("Podaj ilosc: ");
            ilosc = klawiatura.nextInt();

            rzecz = new Item(nazwaRzeczy, cenaRzeczy, ilosc);

            koszyk.add(rzecz);

            System.out.println("Kontynuowac zakupy (t/n)? ");
            kontynuujZakupy = klawiatura.next();

        }
        while (kontynuujZakupy.equals("t"));

        for (int i = 0; i < koszyk.size(); i++) {
            System.out.println(koszyk.get(i).toString());
        }

    }
}